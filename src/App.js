import React, { Component } from 'react';
import Tabela from './components/Tabela';
import { Navbar, Nav } from 'react-bootstrap'
import Tabela2 from './components/Tabela2';
import { Switch, Route } from 'react-router-dom'
import Inicio from './components/Inicio';

//COMPONENTE INICAL, COMO SE FOSSE UM TEMPLATE
class App extends Component {
  render() {
    return (
      <div>
        <Navbar bg="primary" variant="dark">
          <Navbar.Brand href="#">Modelo</Navbar.Brand>
          <Nav className="mr-auto">
            <Nav.Link href="#/exemplo1">Exemplo 1</Nav.Link>
            <Nav.Link href="#/exemplo2">Exemplo 2</Nav.Link >
          </Nav>
        </Navbar>

        <div className="container" style={{marginTop:10}}>
        {/* AQUI FICA AS ROTAS, AO ACESSAR LINK /exemplo1, RENDERIZA Tabela dentro desta <div/> */}
          <Switch>
              <Route exact path={'/exemplo1'} component={Tabela}  />                     
              <Route exact path={'/exemplo2'} component={Tabela2}  />    
              <Route path={'/'} component={Inicio}/>                 
          </Switch>
        </div>
      </div>
    );
  }
}

export default App;
