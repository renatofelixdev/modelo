//AQUI FICA AS VARIAVEIS DO SISTEMA, COM SEU ESTADO INICIAL

//AO RECEBER UMA ACAO VERIFICA QUAL VARIAVEL DEVE SER ALTERADA
//APOS ALTERADA OS COMPONENTES QUE ESTAO ESCUTANDO ELAS SERAO ATUALIZADOS
export function listagem(state = {
    lista: [],
    lista2: []
}, action) {

    if (action.type === 'LISTA') {
        state.lista = action.lista
        return Object.assign({}, state)
    }

    if (action.type === 'LISTA2') {
        state.lista2 = action.lista
        return Object.assign({}, state)
    }

    return state

}