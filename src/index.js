import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import * as serviceWorker from './serviceWorker';

import { HashRouter } from 'react-router-dom'

//configuracao do redux
import { Provider } from 'react-redux'
import thunkMiddleware from 'redux-thunk';
import { combineReducers, createStore, applyMiddleware } from 'redux'

import { listagem } from './reducers/listagem'

const reducers = combineReducers({listagem})
const store = createStore(reducers, applyMiddleware(thunkMiddleware))


ReactDOM.render(
    <Provider store={store}>
        <HashRouter>
            {/* TEMPLATE E ROTAS NO App.js */}
            <App />
        </HashRouter>
    </Provider>
    , document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
