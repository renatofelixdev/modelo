import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Table } from 'react-bootstrap'
import 'bootstrap/dist/css/bootstrap.css';
import API from '../api/API';

class Tabela2 extends Component {

    componentDidMount() {
        this.props.listar()
    }

    render() {
        return (
            <div>
                <h3>API: https://servicodados.ibge.gov.br/api/v1/localidades/regioes</h3>
                <Table striped bordered hover>
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Sigla</th>
                            <th>Nome</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            this.props.lista.map((l, i) => {
                                return (
                                    <tr key={i}>
                                        <td>{i + 1}</td>
                                        <td>{l.sigla}</td>
                                        <td>{l.nome}</td>
                                    </tr>
                                )
                            })
                        }
                    </tbody>
                </Table>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    lista: state.listagem.lista2
})

const mapDispatchToProps = (dispatch) => ({
    listar: () => dispatch(API.listar2())
})

export default connect(mapStateToProps, mapDispatchToProps)(Tabela2)
