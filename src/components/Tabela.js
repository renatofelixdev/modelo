import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Table } from 'react-bootstrap'
import 'bootstrap/dist/css/bootstrap.css';
import API from '../api/API';

class Tabela extends Component {

    //APÓS RENDERIZAR O COMPONENTE, ESSA FUNÇÃO É CHAMADA
    componentDidMount(){
        //FAZ REQUISICAO NA API
        this.props.listar()
    }

    render() {
        return (
            <div>
                <h3>API: https://servicodados.ibge.gov.br/api/v1/localidades/estados/22/municipios</h3>
            <Table striped bordered hover>
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Município</th>
                        <th>Microrregião</th>
                        <th>Mesorregião</th>
                        <th>UF</th>
                        <th>Região</th>
                    </tr>
                </thead>
                <tbody>
                    {/* AQUI ITERA A LISTA E PREENCHE A TABELA */}
                    {
                        this.props.lista.map( (l, i) => {
                            return(
                                <tr key={i}>
                                    <td>{i+1}</td>
                                    <td>{l.nome}</td>
                                    <td>{l.microrregiao.nome}</td>
                                    <td>{l.microrregiao.mesorregiao.nome}</td>
                                    <td>{l.microrregiao.mesorregiao.UF.nome}</td>
                                    <td>{l.microrregiao.mesorregiao.UF.regiao.nome}</td>
                                </tr>
                            )
                        })
                    }
                </tbody>
            </Table>
            </div>
        )
    }
}

// ESCULA A VARIAVEL lista QUE ESTA NO REDUCER (src/reducers)
const mapStateToProps = (state) => ({
    lista: state.listagem.lista
})

// METODO PARA FAZER REQUISICAO NA API
const mapDispatchToProps = (dispatch) => ({
    listar: () => dispatch(API.listar())
})

export default connect(mapStateToProps, mapDispatchToProps)(Tabela)
