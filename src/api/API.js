import { listagem, listagem2 } from "../actions/actions";
import toastr from 'toastr'


export default class API {

    static listar() {
        return dispatch => {

            //fecth : método para realizar requisição, o mesmo retorna uma Promise
            // a Promise é tratada com .then (sucesso) e .catch (erro)
            fetch('https://servicodados.ibge.gov.br/api/v1/localidades/estados/22/municipios')
                .then(response => {
                    // reponse é a resposta da requisição
                    // .ok verifica se o código da resposta é 200
                    // .json transforma a resposta em json, o mesmo retorna uma Promise
                    if (response.ok) {
                        response.json()
                            .then(result => {
                                console.log(result)
                                //AO RECEBER O RESULTADO, DISPARA UMA ACAO (listagem() em src/actions)
                                dispatch(listagem(result))
                                //NOTIFICACAO NA TELA
                                toastr.success('Listagem obtida com sucesso!')
                            })
                            .catch(error => {
                                toastr.error('Não foi possível converter para JSON.')
                            })
                    } else {
                        toastr.error('Não foi possível obter a listagem!')
                    }
                })
                .catch(error => {
                    toastr.error('Não foi possível obter a listagem!')
                })
        }
    }

    static listar2() {
        return dispatch => {

            //fecth : método para realizar requisição, o mesmo retorna uma Promise
            // a Promise é tratada com .then (sucesso) e .catch (erro)
            fetch('https://servicodados.ibge.gov.br/api/v1/localidades/regioes')
                .then(response => {
                    // reponse é a resposta da requisição
                    // .ok verifica se o código da resposta é 200
                    // .json transforma a resposta em json, o mesmo retorna uma Promise
                    if (response.ok) {
                        response.json()
                            .then(result => {
                                console.log(result)
                                //AO RECEBER O RESULTADO, DISPARA UMA ACAO (listagem() em src/actions)
                                dispatch(listagem2(result))
                                //NOTIFICACAO NA TELA
                                toastr.success('Listagem obtida com sucesso!')
                            })
                            .catch(error => {
                                toastr.error('Não foi possível converter para JSON.')
                            })
                    } else {
                        toastr.error('Não foi possível obter a listagem!')
                    }
                })
                .catch(error => {
                    toastr.error('Não foi possível obter a listagem!')
                })
        }
    }
}